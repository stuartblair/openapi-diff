# Swagger 2 / OpenAPI 3 feature support

This is a list of all the Swagger 2 / OpenAPI 3 objects and properties supported by OpenAPI-diff. If something is not on the list, it's not suported (yet).

## Swagger 2

| Field | Supported | Notes |
|---|---|---|
| swagger | yes | |
| info | yes | Also supports x-properties within the info object |
| host | yes | |
| basePath | yes | |
| schemes | yes | |
| ^x- | yes | |

## OpenAPI 3.0.0

| Field | Supported | Notes |
|---|---|---|
| openapi | yes | |
| info | yes | Also supports x-properties within the info object |
| ^x- | yes | |
