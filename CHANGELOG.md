<a name="0.6.1"></a>
## [0.6.1](https://bitbucket.org/atlassian/openapi-diff/compare/0.6.0...0.6.1) (2018-01-23)



<a name="0.6.0"></a>
# [0.6.0](https://bitbucket.org/atlassian/openapi-diff/compare/0.5.0...0.6.0) (2018-01-23)


### Features

* **core:** Load specs from YAML sources ([6d0f5e4](https://bitbucket.org/atlassian/openapi-diff/commits/6d0f5e4))


### Reverts

* unbump changelog dependencies for ci health ([18ae0d8](https://bitbucket.org/atlassian/openapi-diff/commits/18ae0d8))



<a name="0.5.0"></a>
# [0.5.0](https://bitbucket.org/atlassian/openapi-diff/compare/0.4.0...0.5.0) (2017-08-17)


### Features

* support to add/edit/delete Swagger 2's schemes property ([b0a4634](https://bitbucket.org/atlassian/openapi-diff/commits/b0a4634))



<a name="0.4.0"></a>
# [0.4.0](https://bitbucket.org/atlassian/openapi-diff/compare/0.3.0...v0.4.0) (2017-08-01)


### Features

* support to add/edit/delete of Swagger 2's host and basePath props ([d8da76d](https://bitbucket.org/atlassian/openapi-diff/commits/d8da76d))



<a name="0.3.0"></a>
# [0.3.0](https://bitbucket.org/atlassian/openapi-diff/compare/0.2.0...v0.3.0) (2017-07-26)


### Features

* support for editions to the openapi / swagger property ([a8c2fc7](https://bitbucket.org/atlassian/openapi-diff/commits/a8c2fc7))



<a name="0.2.0"></a>
# [0.2.0](https://bitbucket.org/atlassian/openapi-diff/compare/0.1.0...v0.2.0) (2017-07-19)


### Features

* support specs in url format ([11a0396](https://bitbucket.org/atlassian/openapi-diff/commits/11a0396))



<a name="0.1.0"></a>
# [0.1.0](https://bitbucket.org/atlassian/openapi-diff/compare/0.0.1...v0.1.0) (2017-07-07)


### Features

* support for editions to the info object and ^x- properties ([8464ac6](https://bitbucket.org/atlassian/openapi-diff/commits/8464ac6))



<a name="0.0.1"></a>
## 0.0.1 (2017-06-22)


### Features

* say 'hello world!' from OpenAPI-diff :) ([fda73c8](https://bitbucket.org/atlassian/openapi-diff/commits/fda73c8))



