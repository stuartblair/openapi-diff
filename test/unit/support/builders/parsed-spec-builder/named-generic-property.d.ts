export interface NamedGenericProperty {
    name: string;
    originalPath: string[];
    value?: any;
}
